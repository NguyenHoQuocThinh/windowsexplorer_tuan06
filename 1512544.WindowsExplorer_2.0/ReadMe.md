﻿# Đại học khoa học tự nhiên
# -------------------------------------------------
###Họ tên: NGUYỄN HỒ QUỐC THỊNH
###MSSV 1512544
# -------------------------------------------------
#Chức năng đã hoàn thành:
1. Sử dụng được shell để duyệt thư mục theo yêu cầu. Tuy nhiên theo máy tính của em phần thư mục C:\User và C:\Windows thì lại không duyệt được được bên TreeView, khi em vào xem thì thấy trong thư mục có các thư mục ở chế độ private, em chưa biết giải quyết như thế nào.
2. Khi thay đổi kích thước của TreeView thì ListView thay đổi kích thước theo.
3. Bổ sung được thanh status bar để hiển thị kích thước thư mục, tập tin.
4. Lưu lại của sổ chính, tuy nhiên chỉ lưu được phần của sổ cha còn kích thước của TreeView và ListView thì chưa lưu được, dữ liệu kích thước được lưu lại trong file "windowsize.txt"
#Luồng sự kiện chính:
* Người dùng khởi động chương trình, click chọn thư mục bên TreeView hoặc double click vào thư mục bên ListView để truy cập vào thư mục.
* Bấm vào dấu + bên phía TreeView để hiện những thư mục con chứa trong đó
#Luồng sự kiện phụ:
* Nếu người dùng double click vào tập tin bên phần ListView thì chương trình sẽ khởi động tập tin đó
#Link bitbucket: https://NguyenHoQuocThinh@bitbucket.org/NguyenHoQuocThinh/windowsexplorer_tuan06.git


