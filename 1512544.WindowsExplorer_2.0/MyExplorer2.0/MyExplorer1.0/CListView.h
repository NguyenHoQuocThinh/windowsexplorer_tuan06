#pragma once
#include "CDrive.h"
#pragma comment(linker,"\"/manifestdependency:type='win32' name='Microsoft.Windows.Common-Controls' version='6.0.0.0' processorArchitecture='*' publicKeyToken='6595b64144ccf1df' language='*'\"")

// TreeView & ListView
#include <commctrl.h>
#pragma comment(lib, "Comctl32.lib")


class CListView
{
private:
	HWND listView;
	HWND statusBar;
	HWND parent;
public:
	CListView();
	~CListView();
	HWND getListView();
	void Create(HWND, HINSTANCE);
	void CreateStatusBar(HWND, HINSTANCE);
	void tao4Cot();
	void loadDesktop();
	void loadThisPC(CDrive*);
	void chonDoiTuong();
	LPCWSTR GetPath(int);
	LPCWSTR GetCurSelPath();
	void LoadChild(LPCWSTR path, CDrive *drive);
	void LoadFileAndFolder(LPCWSTR);
	LPWSTR GetDateModified(const FILETIME &ftLastWrite);
	void DeleteAll();
	LPWSTR convert(_int64 nSize);
	LPWSTR _GetSize(const WIN32_FIND_DATA &fd);
	void statusBarSize();
	HWND getStatusBar();
};

