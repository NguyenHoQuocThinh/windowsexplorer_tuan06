﻿#include "stdafx.h"
#include "resource.h"
#include <tchar.h>
#include "CListView.h"
#include "SomeThing.h"
#include <shellapi.h>

// TreeView & ListView
#include <commctrl.h>
#pragma comment(lib, "Comctl32.lib")

#include <shlwapi.h>
#pragma comment(lib, "shlwapi.lib")

#define KB 1
#define MB 2
#define GB 3
#define TB 4
#define RADIX 10


CListView::CListView()
{
	listView = NULL;
	parent = NULL;
}


CListView::~CListView()
{
}

void CListView::Create(HWND hWnd, HINSTANCE hInst) {
	listView = CreateWindow(WC_LISTVIEWW, L"", WS_VISIBLE | WS_CHILD | WS_VSCROLL | LVS_REPORT | WS_BORDER, 400, 0, 1100, 700, hWnd, (HMENU)IDC_LISTVIEW, hInst, NULL);
	CreateStatusBar(hWnd, hInst);
}

void CListView::CreateStatusBar(HWND hWnd, HINSTANCE hInst) {
	statusBar = CreateWindow(STATUSCLASSNAME, NULL, WS_CHILD | WS_VISIBLE | SBARS_SIZEGRIP, 0, 0, 0, 0, hWnd, NULL, hInst, NULL);
}

void CListView::statusBarSize() {
	RECT main;
	GetWindowRect(listView, &main);
	int nStatusSize[] = { 400,550, -1 };
	SendMessage(statusBar, SB_SETPARTS, 3, (LPARAM)&nStatusSize);
	MoveWindow(statusBar, 0, 0, main.right, main.bottom, TRUE);
}

HWND CListView::getStatusBar() {
	return statusBar;
}

HWND CListView::getListView() {
	return listView;
}

void CListView::tao4Cot() {
	INITCOMMONCONTROLSEX icex;

	// Ensure that the common control DLL is loaded. 
	icex.dwSize = sizeof(INITCOMMONCONTROLSEX);
	icex.dwICC = ICC_LISTVIEW_CLASSES | ICC_TREEVIEW_CLASSES;
	InitCommonControlsEx(&icex);

	LVCOLUMN lvCol;
	lvCol.mask = LVCF_TEXT | LVCF_FMT | LVCF_WIDTH;
	lvCol.fmt = LVCFMT_CENTER;
	// cột 1;
	lvCol.cx = 250;
	lvCol.pszText = L"Tên";
	ListView_InsertColumn(listView, 0, &lvCol);
	// cột 2;
	lvCol.cx = 100;
	lvCol.pszText = L"Loại";
	ListView_InsertColumn(listView, 1, &lvCol);
	// cột 3;
	lvCol.fmt = LVCFMT_LEFT;
	lvCol.cx = 150;
	lvCol.pszText = L"Thời Gian";
	ListView_InsertColumn(listView, 2, &lvCol);
	// Cột 4
	lvCol.fmt = LVCFMT_LEFT;
	lvCol.cx = 150;
	lvCol.pszText = L"Kích thước";
	ListView_InsertColumn(listView, 3, &lvCol);


}

void CListView::loadDesktop() {
	LVITEM lvItem;
	lvItem.mask = LVIF_TEXT | LVIF_PARAM;
	lvItem.iItem = 0;
	lvItem.iSubItem = 0;
	lvItem.pszText = L"This PC";
	lvItem.lParam = (LPARAM)L"This PC";
	ListView_InsertItem(listView, &lvItem);
}

void CListView::loadThisPC(CDrive *drive) {
	tao4Cot();
	ListView_DeleteAllItems(listView);
	LV_ITEM lvItem;
	for (int i = 0; i < drive->GetCount(); i++) {
		lvItem.mask = LVIF_TEXT | LVIF_PARAM;
		lvItem.iItem = i;
		lvItem.iSubItem = 0;
		lvItem.pszText = drive->GetDisplayName(i);
		lvItem.lParam = (LPARAM)drive->GetDriveName(i);
		ListView_InsertItem(listView, &lvItem);
	}

	TCHAR *buffer = new TCHAR[34];
	wsprintf(buffer, _T("My Computer có tổng cộng %d ổ đĩa"), drive->GetCount());
	SendMessage(statusBar, SB_SETTEXT, 0, (LPARAM)buffer);
}

void CListView::LoadFileAndFolder(LPCWSTR path) {
	ListView_DeleteAllItems(listView);
	LPSHELLFOLDER pszDrive = NULL;
	LPITEMIDLIST pidl = NULL;
	HRESULT hr = NULL;
	LPENUMIDLIST penumIDL = NULL;
	pidl = ILCreateFromPath(path);
	hr = SHBindToObject(NULL, pidl, NULL, IID_IShellFolder, (void**)&pszDrive);
	LV_ITEM lv;
	TCHAR * folderPath = NULL;
	int nItemCount = 0;
	int folderCount = 0;
	// Load thư mục
	pszDrive->EnumObjects(NULL, SHCONTF_FOLDERS, &penumIDL);
	do {
		hr = penumIDL->Next(1, &pidl, NULL);
		if (hr == S_OK) {
			WIN32_FIND_DATA fd;
			SHGetDataFromIDList(pszDrive, pidl, SHGDFIL_FINDDATA, &fd, sizeof(WIN32_FIND_DATA));
			folderPath = new TCHAR[wcslen(path) + wcslen(fd.cFileName) + 2];
			StrCpy(folderPath, path);
			if (wcslen(path) != 3)
				StrCat(folderPath, _T("\\"));
			StrCat(folderPath, fd.cFileName);
			lv.mask = LVIF_TEXT | LVIF_PARAM;
			lv.iItem = nItemCount;
			lv.iSubItem = 0;
			lv.pszText = fd.cFileName;
			lv.lParam = (LPARAM)folderPath;
			ListView_InsertItem(listView, &lv);
			ListView_SetItemText(listView, nItemCount, 1, L"Thư mục");
			ListView_SetItemText(listView, nItemCount, 2, GetDateModified(fd.ftLastWriteTime));
			nItemCount++;
			folderCount++;
		}
	} while (hr == S_OK);
	// Load file
	TCHAR *filePath;
	DWORD fileSize = 0;
	int fileCount = 0;
	pszDrive->EnumObjects(NULL, SHCONTF_NONFOLDERS, &penumIDL);
	do {
		hr = penumIDL->Next(1, &pidl, NULL);
		if (hr == S_OK) {
			WCHAR buffer[1024];
			STRRET strret;
			pszDrive->GetDisplayNameOf(pidl, SHGDN_NORMAL, &strret);
			WCHAR *buffer2;
			WIN32_FIND_DATA fd;
			SHGetDataFromIDList(pszDrive, pidl, SHGDFIL_FINDDATA, &fd, sizeof(WIN32_FIND_DATA));

			StrRetToBuf(&strret, pidl, buffer, 1024);
			filePath = new TCHAR[wcslen(path) + wcslen(fd.cFileName) + 2];
			StrCpy(filePath, path);
			if (wcslen(path) != 3)
				StrCat(filePath, _T("\\"));
			StrCat(filePath, fd.cFileName);
			lv.mask = LVIF_TEXT | LVIF_PARAM;
			lv.iItem = nItemCount;
			lv.iSubItem = 0;
			lv.pszText = fd.cFileName;
			lv.lParam = (LPARAM)filePath;
			ListView_InsertItem(listView, &lv);

			ListView_SetItemText(listView, nItemCount, 1, _T("Tệp tin"));
			ListView_SetItemText(listView, nItemCount, 2, GetDateModified(fd.ftLastWriteTime));
			ListView_SetItemText(listView, nItemCount, 3, _GetSize(fd));
			fileSize = fileSize + fd.nFileSizeLow;
			nItemCount++;
			fileCount++;
		}
	} while (hr == S_OK);


	TCHAR *info = new TCHAR[256];
	
	if (wcslen(path) == 3) {
		wsprintf(info, _T("Ổ đĩa có %d thư mục con và %d tập tin. (Không tính ẩn và hệ thống)"), folderCount, fileCount);
		SendMessage(statusBar, SB_SETTEXT, 0, (LPARAM)info);
	}
	else {
		wsprintf(info, _T("Thư mục có %d thư mục con và %d tập tin. (Không tính ẩn và hệ thống)"), folderCount, fileCount);
		SendMessage(statusBar, SB_SETTEXT, 0, (LPARAM)info);
		SendMessage(statusBar, SB_SETTEXT, 1, (LPARAM)L"Kích Thước thư mục: ");
		SendMessage(statusBar, SB_SETTEXT, 2, (LPARAM)convert(fileSize));
	}


}

LPCWSTR CListView::GetPath(int iItem) {
	LVITEM lv;
	lv.mask = LVIF_PARAM;
	lv.iItem = iItem;
	lv.iSubItem = 0;
	ListView_GetItem(listView, &lv);
	return (LPCWSTR)lv.lParam;
}

LPCWSTR CListView::GetCurSelPath() {
	return GetPath(ListView_GetSelectionMark(listView));
}

void CListView::chonDoiTuong() {
	LPCWSTR path = GetCurSelPath();
	WIN32_FIND_DATA fd;
	GetFileAttributesEx(path, GetFileExInfoStandard, &fd);

	if (fd.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY)
	{
		ListView_DeleteAllItems(listView);
		LoadFileAndFolder(path);
	}
	else
		ShellExecute(NULL, L"open", path, NULL, NULL, SW_SHOWNORMAL);
}

LPWSTR CListView::GetDateModified(const FILETIME &ftLastWrite) {
	//Chuyển đổi sang local time
	SYSTEMTIME stUTC, stLocal;
	FileTimeToSystemTime(&ftLastWrite, &stUTC);
	SystemTimeToTzSpecificLocalTime(NULL, &stUTC, &stLocal);

	TCHAR *buffer = new TCHAR[50];
	wsprintf(buffer, _T("%02d/%02d/%04d %02d:%02d %s"),
		stLocal.wDay, stLocal.wMonth, stLocal.wYear,
		(stLocal.wHour>12) ? (stLocal.wHour / 12) : (stLocal.wHour),
		stLocal.wMinute,
		(stLocal.wHour>12) ? (_T("Chiều")) : (_T("Sáng")));

	return buffer;
}

void CListView::LoadChild(LPCWSTR path, CDrive *drive)
{
	if (path == NULL)
		return;

	if (!StrCmp(path, _T("Desktop")))
		loadDesktop();
	else
		if (!StrCmp(path, _T("MyComputer")))
			loadThisPC(drive);
		else
			LoadFileAndFolder(path);
}

void CListView::DeleteAll()
{
	ListView_DeleteAllItems(listView);
}

LPWSTR CListView::_GetSize(const WIN32_FIND_DATA &fd)
{
	DWORD dwSize = fd.nFileSizeLow;

	return convert(dwSize);
}

LPWSTR CListView::convert(_int64 nSize) {
	int nType = 0; //Bytes

	while (nSize >= 1048576) //
	{
		nSize /= 1024;
		++nType;
	}

	__int64 nRight;

	if (nSize >= 1024)
	{
		//Lấy một chữ số sau thập phân của nSize chứa trong nRight
		nRight = nSize % 1024;

		while (nRight > 99)
			nRight /= 10;

		nSize /= 1024;
		++nType;
	}
	else
		nRight = 0;

	TCHAR *buffer = new TCHAR[11];
	_itow_s(nSize, buffer, 11, RADIX);

	if (nRight != 0 && nType > KB)
	{
		StrCat(buffer, _T("."));
		TCHAR *right = new TCHAR[3];
		_itow_s(nRight, right, 3, RADIX);
		StrCat(buffer, right);
	}

	switch (nType)
	{
	case 0://Bytes
		StrCat(buffer, _T(" bytes"));
		break;
	case KB:
		StrCat(buffer, _T(" KB"));
		break;
	case MB:
		StrCat(buffer, _T(" MB"));
		break;
	case GB:
		StrCat(buffer, _T(" GB"));
		break;
	case TB:
		StrCat(buffer, _T(" TB"));
		break;
	}

	return buffer;
}

