﻿#include "stdafx.h"
#include "CTreeView.h"
#include "Resource.h"
#include "SomeThing.h"
#include "ObjIdl.h"
#pragma comment(linker,"\"/manifestdependency:type='win32' name='Microsoft.Windows.Common-Controls' version='6.0.0.0' processorArchitecture='*' publicKeyToken='6595b64144ccf1df' language='*'\"")

// TreeView & ListView
#include <commctrl.h>
#pragma comment(lib, "Comctl32.lib")

#include <shlwapi.h>
#pragma comment(lib, "shlwapi.lib")


#define MAXPATHLEN 10240;

#define IDI_FLOPPY 4
#define IDI_USB 5
#define IDI_HDD 6
#define IDI_CD  7 

CTreeView::CTreeView()
{
}


CTreeView::~CTreeView()
{
}

void CTreeView::Create(HWND hWnd, HINSTANCE hInst) {
	treeView = CreateWindow(WC_TREEVIEWW, L"", WS_CHILD | WS_VISIBLE | WS_VSCROLL | WS_BORDER | TVS_HASLINES | TVS_LINESATROOT | TVS_HASBUTTONS, 0,0, 400, 700, hWnd, (HMENU)IDC_TREEVIEW, hInst, NULL);
}

HWND CTreeView::getTreeView() {
	return treeView;
}

void CTreeView::loadThisPC(CDrive *drive) {
	TV_INSERTSTRUCT tvInsert;
	tvInsert.item.mask = TVIF_TEXT | TVIF_PARAM;
	// Desktop
	tvInsert.hParent = NULL;
	tvInsert.hInsertAfter = TVI_ROOT;
	tvInsert.item.pszText = L"Desktop";
	tvInsert.item.lParam = (LPARAM)L"Desktop";
	HTREEITEM hDesktop = TreeView_InsertItem(treeView, &tvInsert);
	// This PC
	tvInsert.hParent = hDesktop;
	tvInsert.hInsertAfter = TVI_LAST;
	tvInsert.item.pszText = L"This PC";
	tvInsert.item.lParam = (LPARAM)L"This PC";
	HTREEITEM hThisPC = TreeView_InsertItem(treeView, &tvInsert);

	TreeView_Expand(treeView, hThisPC, TVE_EXPAND);
	TreeView_SelectItem(treeView, hThisPC);

	for (int i = 0; i < drive->GetCount(); i++) {
		tvInsert.hParent = hThisPC;
		tvInsert.item.pszText = drive->GetDisplayName(i);
		tvInsert.item.lParam = (LPARAM)drive->GetDriveName(i);
		HTREEITEM hDrive = TreeView_InsertItem(treeView, &tvInsert);

		if ((drive->GetIconIndex(i) == IDI_HDD) || (drive->GetIconIndex(i) == IDI_USB))
		{
			tvInsert.hParent = hDrive; //Them
			tvInsert.item.pszText = _T("PreLoad"); //Them
			tvInsert.item.lParam = (LPARAM)_T("PreLoad");
			TreeView_InsertItem(treeView, &tvInsert);
		}
	}

	TreeView_Expand(treeView, hThisPC, TVE_EXPAND);
	TreeView_SelectItem(treeView, hThisPC);
}

LPCWSTR CTreeView::GetPath(HTREEITEM hItem) {
	TVITEMEX tv;
	tv.mask = TVIF_PARAM;
	tv.hItem = hItem;
	TreeView_GetItem(treeView, &tv);
	return (LPCWSTR)tv.lParam;
}

LPCWSTR CTreeView::GetCurPath()
{
	return GetPath(GetCurSel());
}

void CTreeView::Expand(HTREEITEM hItem) {
	TreeView_Expand(treeView, hItem, TVE_EXPAND);
}

HTREEITEM CTreeView::GetCurSel() {
	return TreeView_GetNextItem(treeView, NULL, TVGN_CARET);
}

HTREEITEM CTreeView::GetDesktop() {
	return TreeView_GetRoot(treeView);
}

HTREEITEM CTreeView::GetMyComputer() {
	return TreeView_GetChild(treeView, GetDesktop());
}

void CTreeView::PreLoad(HTREEITEM hItem) {
	LPITEMIDLIST pidl = NULL;
	LPSHELLFOLDER preload = NULL;
	HRESULT hr = NULL;
	LPENUMIDLIST penumIDL = NULL;
	TCHAR buffer[10240];
	StrCpy(buffer, GetPath(hItem));
	pidl = ILCreateFromPathW(GetPath(hItem));
	hr = SHBindToObject(NULL, pidl, NULL, IID_IShellFolder, (void**)&preload);
	preload->EnumObjects(NULL, SHCONTF_FOLDERS, &penumIDL);
	hr = penumIDL->Next(1, &pidl, NULL);
	if (hr == S_OK) {
		TCHAR a[1024];
		STRRET strret;
		preload->GetDisplayNameOf(pidl, SHGDN_NORMAL, &strret);
		StrRetToBuf(&strret, pidl, a, 1024);
		TV_INSERTSTRUCT tvInsert;
		tvInsert.hParent = hItem;
		tvInsert.hInsertAfter = TVI_LAST;
		tvInsert.item.mask = TVIF_TEXT | TVIF_PARAM;
		tvInsert.item.pszText = NULL;
		tvInsert.item.lParam = (LPARAM)_T("PreLoad");
		TreeView_InsertItem(treeView, &tvInsert);
		CoTaskMemFree(pidl);
	}
}

void CTreeView::LoadChild(HTREEITEM &hParent, LPCWSTR path, BOOL bShowHiddenSystem) {
	LPSHELLFOLDER pszDrive = NULL;
	LPITEMIDLIST pidl = NULL;
	HRESULT hr = NULL;
	LPENUMIDLIST penumIDL = NULL;

	pidl = ILCreateFromPath(path);

	hr = SHBindToObject(NULL, pidl, NULL, IID_IShellFolder, (void**)&pszDrive);

	pszDrive->EnumObjects(NULL, SHCONTF_FOLDERS, &penumIDL);
	if (hr != S_OK)
		return;
	TV_INSERTSTRUCT tvInsert;
	tvInsert.hParent = hParent;
	tvInsert.hInsertAfter = TVI_LAST;
	tvInsert.item.mask = TVIF_TEXT | TVIF_PARAM;
	TCHAR *folderPath = NULL;
	do {
		hr = penumIDL->Next(1, &pidl, NULL);
		if (hr == S_OK) {
			WCHAR buffer[1024];
			STRRET strret;
			pszDrive->GetDisplayNameOf(pidl, SHGDN_NORMAL, &strret);
			StrRetToBuf(&strret, pidl, buffer, 1024);
			folderPath = new TCHAR[wcslen(path) + wcslen(buffer) + 2];
			StrCpy(folderPath, path);
			if (wcslen(path) != 3)
				StrCat(folderPath, _T("\\"));
			StrCat(folderPath, buffer);
			tvInsert.item.pszText = buffer;
			tvInsert.item.lParam = (LPARAM)folderPath;
			HTREEITEM hItem = TreeView_InsertItem(treeView, &tvInsert);
			PreLoad(hItem);
			//LoadFromDrive(hItem, path);
			CoTaskMemFree(pidl);
		}
	} while (hr == S_OK);
}

void CTreeView::PreloadExpanding(HTREEITEM hPrev, HTREEITEM hCurSel) {
	if (hCurSel == GetMyComputer())
		return;

	HTREEITEM hCurSelChild = TreeView_GetChild(treeView, hCurSel);

	if (!StrCmp(GetPath(hCurSelChild), _T("PreLoad")))
	{
		TreeView_DeleteItem(treeView, hCurSelChild);
		LoadChild(hCurSel, GetPath(hCurSel));
	}
}

void CTreeView::LoadFromDrive(HTREEITEM parent, LPCWSTR path) {
	/*LPITEMIDLIST pidl = NULL;
	LPSHELLFOLDER preload = NULL;
	HRESULT hr = NULL;
	LPENUMIDLIST penumIDL = NULL;
	TCHAR buffer[10240];
	StrCpy(buffer, GetPath(parent));
	pidl = ILCreateFromPathW(GetPath(parent));
	hr = SHBindToObject(NULL, pidl, NULL, IID_IShellFolder, (void**)&preload);
	preload->EnumObjects(NULL, SHCONTF_FOLDERS, &penumIDL);
	hr = penumIDL->Next(1, &pidl, NULL);
	if (hr == S_OK) {
		TCHAR a[1024];
		STRRET strret;
		preload->GetDisplayNameOf(pidl, SHGDN_NORMAL, &strret);
		StrRetToBuf(&strret, pidl, a, 1024);
		TV_INSERTSTRUCT tvInsert;
		tvInsert.hParent = parent;
		tvInsert.hInsertAfter = TVI_LAST;
		tvInsert.item.mask = TVIF_TEXT | TVIF_PARAM;
		tvInsert.item.pszText = NULL;
		tvInsert.item.lParam = (LPARAM)_T("PreLoad");
		TreeView_InsertItem(treeView, &tvInsert);
		CoTaskMemFree(pidl);
	}*/
}