#ifndef CTREEVIEW_H
#define CTREEVIEW_H

#include "stdafx.h"
#include <windows.h>
#include <tchar.h>

#include <commctrl.h>
#pragma comment(lib, "comctl32.lib")

#include "CDrive.h"

class CTreeView
{
private:
	HWND treeView;
public:
	CTreeView();
	~CTreeView();

	HWND getTreeView();
	HTREEITEM	GetDesktop();
	HTREEITEM	GetMyComputer();
	LPCWSTR		GetPath(HTREEITEM hItem);
	HTREEITEM	GetCurSel();
	LPCWSTR		GetCurPath();

	void Create(HWND hWnd, HINSTANCE hInst);
	void loadThisPC(CDrive *);
	void LoadChild(HTREEITEM &hParent, LPCWSTR path, BOOL bShowHiddenSystem = FALSE);
	void PreloadExpanding(HTREEITEM hPrev, HTREEITEM hCurSel);
	void PreLoad(HTREEITEM hItem);
	void Expand(HTREEITEM hItem);

	void LoadFromDrive(HTREEITEM parent, LPCWSTR path);
};

#endif