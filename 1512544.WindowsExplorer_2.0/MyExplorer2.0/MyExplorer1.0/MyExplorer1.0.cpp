﻿#include "stdafx.h"
#include "MyExplorer1.0.h"
#include "CDrive.h"
#include "CTreeView.h"
#include "CListView.h"
#include <WindowsX.h>
#include <stdlib.h>
#include <malloc.h>
#include <memory.h>
#include <tchar.h>
#include <commctrl.h>
#include "Resource.h"
#include "macros.h"
#include "SomeThing.h"
#include <iostream>
#include <fstream>
using namespace std;

#include <shlwapi.h>
#pragma comment(lib, "shlwapi.lib")

#define IDI_FLOPPY 4
#define IDI_USB 5
#define IDI_HDD 6
#define IDI_CD  7 

#define MAX_LOADSTRING 100

// Global Variables:
HINSTANCE hInst;                                // current instance
WCHAR szTitle[MAX_LOADSTRING];                  // The title bar text
WCHAR szWindowClass[MAX_LOADSTRING];            // the main window class name
CListView* listView;
CDrive* drive;
CTreeView* treeView;
HTREEITEM hRoot;
bool loading = false;
HWND g_TreeView;
HWND g_ListView;
HWND g_StatusBar;
RECT g_tree;




void OnDestroy(HWND hwnd);
BOOL OnCreate(HWND hWnd, LPCREATESTRUCT lpCreateStruct);
void OnCommand(HWND hWnd, int id, HWND hwndCtl, UINT codeNotify);
LRESULT OnNotify(HWND hwnd, int idFrom, NMHDR *pnm);

// Forward declarations of functions included in this code module:
ATOM                MyRegisterClass(HINSTANCE hInstance);
BOOL                InitInstance(HINSTANCE, int);
LRESULT CALLBACK    WndProc(HWND, UINT, WPARAM, LPARAM);
INT_PTR CALLBACK    About(HWND, UINT, WPARAM, LPARAM);




int APIENTRY wWinMain(_In_ HINSTANCE hInstance,
                     _In_opt_ HINSTANCE hPrevInstance,
                     _In_ LPWSTR    lpCmdLine,
                     _In_ int       nCmdShow)
{
    UNREFERENCED_PARAMETER(hPrevInstance);
    UNREFERENCED_PARAMETER(lpCmdLine);

    // TODO: Place code here.

    // Initialize global strings
    LoadStringW(hInstance, IDS_APP_TITLE, szTitle, MAX_LOADSTRING);
    LoadStringW(hInstance, IDC_MYEXPLORER10, szWindowClass, MAX_LOADSTRING);
    MyRegisterClass(hInstance);

    // Perform application initialization:
    if (!InitInstance (hInstance, nCmdShow))
    {
        return FALSE;
    }

    HACCEL hAccelTable = LoadAccelerators(hInstance, MAKEINTRESOURCE(IDC_MYEXPLORER10));

    MSG msg;

    // Main message loop:
    while (GetMessage(&msg, nullptr, 0, 0))
    {
        if (!TranslateAccelerator(msg.hwnd, hAccelTable, &msg))
        {
            TranslateMessage(&msg);
            DispatchMessage(&msg);
        }
    }

    return (int) msg.wParam;
}



//
//  FUNCTION: MyRegisterClass()
//
//  PURPOSE: Registers the window class.
//
ATOM MyRegisterClass(HINSTANCE hInstance)
{
    WNDCLASSEXW wcex;

    wcex.cbSize = sizeof(WNDCLASSEX);

    wcex.style          = CS_HREDRAW | CS_VREDRAW;
    wcex.lpfnWndProc    = WndProc;
    wcex.cbClsExtra     = 0;
    wcex.cbWndExtra     = 0;
    wcex.hInstance      = hInstance;
    wcex.hIcon          = LoadIcon(hInstance, MAKEINTRESOURCE(IDI_ICON1));
    wcex.hCursor        = LoadCursor(nullptr, IDC_ARROW);
    wcex.hbrBackground  = (HBRUSH)(COLOR_BTNFACE+1);
    wcex.lpszMenuName   = MAKEINTRESOURCEW(IDC_MYEXPLORER10);
    wcex.lpszClassName  = szWindowClass;
    wcex.hIconSm        = LoadIcon(wcex.hInstance, MAKEINTRESOURCE(IDI_ICON1));

    return RegisterClassExW(&wcex);
}

//
//   FUNCTION: InitInstance(HINSTANCE, int)
//
//   PURPOSE: Saves instance handle and creates main window
//
//   COMMENTS:
//
//        In this function, we save the instance handle in a global variable and
//        create and display the main program window.
//
BOOL InitInstance(HINSTANCE hInstance, int nCmdShow)
{
   hInst = hInstance; // Store instance handle in our global variable

   ifstream file;
   file.open("windowsize.txt");
   RECT main;
   file >> main.top;
   file >> main.left;
   file >> main.bottom;
   file >> main.right;
 
   HWND hWnd = CreateWindowW(szWindowClass, L"1512544_WindowsExplorer_2.0", WS_OVERLAPPEDWINDOW,
     main.left, main.top, main.right-main.left, main.bottom-main.top, nullptr, nullptr, hInstance, nullptr);
   if (!hWnd)
   {
      return FALSE;
   }

   ShowWindow(hWnd, nCmdShow);
   UpdateWindow(hWnd);

   return TRUE;
}

LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{	
	static int nleftWnd_width = LEFT_WINDOW_WIDTH;
	static BOOL xSizing;
	static HCURSOR	hcSizeEW = NULL;
	hcSizeEW = LoadCursor(NULL, IDC_SIZEWE);
	RECT rect;
    switch (message)
    {
		HANDLE_MSG(hWnd, WM_DESTROY, OnDestroy);
		HANDLE_MSG(hWnd, WM_CREATE, OnCreate);
		HANDLE_MSG(hWnd, WM_COMMAND, OnCommand);
		HANDLE_MSG(hWnd, WM_NOTIFY, OnNotify);
		case WM_LBUTTONDOWN: {
			int xPos;
			int yPos;

			xPos = (int)LOWORD(lParam);
			yPos = (int)HIWORD(lParam);

			xSizing = (xPos > nleftWnd_width - SPLITTER_BAR_WIDTH && xPos < nleftWnd_width + SPLITTER_BAR_WIDTH);
			if (xSizing) {
				SetCapture(hWnd);
				if (xSizing) {
					SetCursor(hcSizeEW);
				}
			}
		}break;
		case WM_LBUTTONUP: {
			if (xSizing)
			{
				RECT    focusrect;
				HDC     hdc;

				// Releases the captured mouse input
				ReleaseCapture();
				// Get the main window dc to draw a focus rectangle
				hdc = GetDC(hWnd);
				GetClientRect(hWnd, &rect);
				if (xSizing)
				{
					SetRect(&focusrect, nleftWnd_width - (WIDTH_ADJUST * 2), rect.top + TOP_POS,
						nleftWnd_width + WIDTH_ADJUST,
						rect.bottom - 80);

					// Call api to vanish the dragging rectangle 
					DrawFocusRect(hdc, &focusrect);

					xSizing = FALSE;

				}
				// Release the dc once done 
				ReleaseDC(hWnd, hdc);
			}
			// Post a WM_SIZE message to redraw the windows
			PostMessage(hWnd, WM_SIZE, 0, 0);
		}break;
		case WM_MOUSEMOVE: {
			int   xPos;
			int   yPos;

			// Get the x and y co-ordinates of the mouse
			xPos = (int)LOWORD(lParam);
			yPos = (int)HIWORD(lParam);

			if (xPos < LEFT_MINIMUM_SPACE || xPos > RIGHT_MINIMUM_SPACE)
			{
				return 0;
			}

			// Checks if the left button is pressed during dragging the splitter
			if (wParam == MK_LBUTTON)
			{
				// If the window is d`agged using the splitter, get the
				// cursors current postion and draws a focus rectangle 
				if (xSizing)
				{
					RECT    focusrect;
					HDC     hdc;

					hdc = GetDC(hWnd);
					GetClientRect(hWnd, &rect);

					if (xSizing)
					{
						SetRect(&focusrect, nleftWnd_width - (WIDTH_ADJUST * 2), rect.top + TOP_POS,
							nleftWnd_width + WIDTH_ADJUST,
							rect.bottom - BOTTOM_POS);

						DrawFocusRect(hdc, &focusrect);

						// Get the size of the left window to increase
						nleftWnd_width = xPos;

						// Draws a focus rectangle
						SetRect(&focusrect, nleftWnd_width - (SPLITTER_BAR_WIDTH * 2), rect.top + 80,
							nleftWnd_width + SPLITTER_BAR_WIDTH,
							rect.bottom - BOTTOM_POS);

						DrawFocusRect(hdc, &focusrect);

					}
					ReleaseDC(hWnd, hdc);
				}
			}
			// Set the cursor image to east west direction when the mouse is over 
			// the splitter window
			if ((xPos > nleftWnd_width - SPLITTER_BAR_WIDTH && xPos < nleftWnd_width +
				SPLITTER_BAR_WIDTH))
			{
				SetCursor(hcSizeEW);
			}
		}break;
		case WM_SIZE: {
			GetClientRect(hWnd, &rect);
			listView->statusBarSize();

			MoveWindow(g_TreeView, rect.left,
				rect.top,
				rect.left + (nleftWnd_width - WIDTH_ADJUST),
				(rect.bottom - 30),
				FALSE);
			MoveWindow(g_ListView, rect.left + nleftWnd_width + WIDTH_ADJUST,
				rect.top,
				rect.right - (nleftWnd_width + WIDTH_ADJUST),
				rect.bottom - 30,
				FALSE);
			InvalidateRect(hWnd, &rect, TRUE);
		}break;
    default:
        return DefWindowProc(hWnd, message, wParam, lParam);
    }
    return 0;
	
}

// Message handler for about box.
INT_PTR CALLBACK About(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{	
    UNREFERENCED_PARAMETER(lParam);
    switch (message)
    {
    case WM_INITDIALOG:
        return (INT_PTR)TRUE;

    case WM_COMMAND:
        if (LOWORD(wParam) == IDOK || LOWORD(wParam) == IDCANCEL)
        {
            EndDialog(hDlg, LOWORD(wParam));
            return (INT_PTR)TRUE;
        }
        break;
    }
    return (INT_PTR)FALSE;
}

void OnDestroy(HWND hwnd) {
	ofstream file;
	file.open("windowsize.txt");
	RECT main;
	GetWindowRect(hwnd, &main);
	file << main.top << endl;
	file << main.left << endl;
	file << main.bottom << endl;
	file << main.right << endl;
	file.close();
	PostQuitMessage(0);
}

BOOL OnCreate(HWND hWnd, LPCREATESTRUCT lpCreateStruct) {
	listView = new CListView();
	drive = new CDrive();
	treeView = new CTreeView();

	drive->GetSystemDrives();

	treeView->Create(hWnd, hInst);
	treeView->loadThisPC(drive);
	g_TreeView = treeView->getTreeView();

	listView->Create(hWnd, hInst);
	listView->loadThisPC(drive);
	g_ListView = listView->getListView();
	g_StatusBar = listView->getStatusBar();
	return TRUE;
}

LRESULT OnNotify(HWND hwnd, int idFrom, NMHDR *pnm) {

	LPNMTREEVIEW lpnmTree = (LPNMTREEVIEW)pnm;
	switch (idFrom) {
		case IDC_LISTVIEW: {
			if (pnm->code == NM_DBLCLK)
				listView->chonDoiTuong();
		}break;
		case IDC_TREEVIEW: {
			switch (pnm->code) {
				case TVN_ITEMEXPANDING: {
					treeView->PreloadExpanding(lpnmTree->itemOld.hItem, lpnmTree->itemNew.hItem);
				}break;
				case TVN_SELCHANGED: {
					treeView->Expand(treeView->GetCurSel());
					listView->DeleteAll();
					listView->LoadChild(treeView->GetCurPath(), drive);
				}break;
			}
		}break;
	}
	return 0;
}
CSomeThing g_SomeThing;

void OnCommand(HWND hWnd, int id, HWND hwndCtl, UINT codeNotify) {
	switch (id)
	{
	case ID_SHELL_TEST:
		g_SomeThing.testing();
		break;
	case IDM_ABOUT:
		MessageBox(0, L"Nguyễn Hồ Quốc Thịnh\nMSSV: 1512544\nUpdate: Shell-Splitter-StatusBar-Size", L"Thông tin", 0);
		break;
	case IDM_EXIT:
		DestroyWindow(hWnd);
		break;
	}
}
